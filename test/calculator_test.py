import unittest
import test.calculator_test
import csv
from paydaycalculator.calculator import Calculator

class CalculatorTests(unittest.TestCase):

    def test_2020(self):
        expected = [
            ["Reminder date","Payday date"],
            ["2020-01-07","2020-01-10"],
            ["2020-02-05","2020-02-10"],
            ["2020-03-05","2020-03-10"],
            ["2020-04-06","2020-04-09"],
            ["2020-05-05","2020-05-08"],
            ["2020-06-05","2020-06-10"],
            ["2020-07-07","2020-07-10"],
            ["2020-08-05","2020-08-10"],
            ["2020-09-07","2020-09-10"],
            ["2020-10-06","2020-10-09"],
            ["2020-11-05","2020-11-10"],
            ["2020-12-07","2020-12-10"]
        ]
        Calculator().calculate(2020, "2020test")

        lines = []

        with open('2020test.csv', newline='') as csvfile:
             reader = csv.reader(csvfile, delimiter=';')
             for row in reader:
                lines.append(row)

        self.assertCountEqual(expected, lines)


    def test_2007(self):
        expected = [
            ["Reminder date","Payday date"],
            ["2007-01-05","2007-01-10"],
            ["2007-02-06","2007-02-09"],
            ["2007-03-06","2007-03-09"],
            ["2007-04-04","2007-04-10"],
            ["2007-05-07","2007-05-10"],
            ["2007-06-05","2007-06-08"],
            ["2007-07-05","2007-07-10"],
            ["2007-08-07","2007-08-10"],
            ["2007-09-05","2007-09-10"],
            ["2007-10-05","2007-10-10"],
            ["2007-11-06","2007-11-09"],
            ["2007-12-05","2007-12-10"]
        ]
        
        Calculator().calculate(2007, "2007test")

        lines = []

        with open('2007test.csv', newline='') as csvfile:
             reader = csv.reader(csvfile, delimiter=';')
             for row in reader:
                lines.append(row)
        
        self.assertCountEqual(expected, lines)

if __name__ == '__main__':
    unittest.main()