import unittest
import test.holiday_test
from paydaycalculator import holidays
from datetime import date

class HolidayTests(unittest.TestCase):

    def test_calculateEasterSunday2022(self):
        self.assertEqual(holidays.calculateEasterSunday(2022), date(2022, 4, 17))

    def test_calculateEasterSunday2007(self):
        self.assertEqual(holidays.calculateEasterSunday(2007), date(2007, 4, 8))

    def test_calculateEasterSunday1997(self):
        self.assertEqual(holidays.calculateEasterSunday(1999), date(1999, 4, 4))

    def test_calculateHolidays2003(self):
        holidayList = []
        holidayList.append(date(2003, 1, 1))    # newyears
        holidayList.append(date(2003, 2, 24))   # independence day
        holidayList.append(date(2003, 4, 18))   # good friday
        holidayList.append(date(2003, 4, 20))   # easter
        holidayList.append(date(2003, 5, 1))    # spring holiday
        holidayList.append(date(2003, 6, 8))    # pentecost
        holidayList.append(date(2003, 8, 30))   # second independence day
        holidayList.append(date(2003, 12, 24))  # christmas saturday
        holidayList.append(date(2003, 12, 25))  # first christmas holiday
        holidayList.append(date(2003, 12, 26))  # second christmas holiday

        self.assertCountEqual(holidays.getHolidays(2003), holidayList)

    def test_calculateHolidays2022(self):
        holidayList = []
        holidayList.append(date(2022, 1, 1))    # newyears
        holidayList.append(date(2022, 2, 24))   # independence day
        holidayList.append(date(2022, 4, 15))   # good friday
        holidayList.append(date(2022, 4, 17))   # easter
        holidayList.append(date(2022, 5, 1))    # spring holiday
        holidayList.append(date(2022, 6, 5))    # pentecost
        holidayList.append(date(2022, 8, 30))   # second independence day
        holidayList.append(date(2022, 12, 24))  # christmas saturday
        holidayList.append(date(2022, 12, 25))  # first christmas holiday
        holidayList.append(date(2022, 12, 26))  # second christmas holiday

        self.assertCountEqual(holidays.getHolidays(2022), holidayList)

if __name__ == '__main__':
    unittest.main()