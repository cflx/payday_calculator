from datetime import date, timedelta

def calculateEasterSunday(year : int):
    # Adapted to Python from source:
    # https://stackoverflow.com/a/2510411
    g = year % 19
    c = year / 100
    h = int((c - int(c / 4) - int((8 * c + 13) / 25) + 19 * g + 15) % 30)
    i = int(h - int(h / 28) * (1 - int(h / 28) * int(29 / (h + 1)) * int((21 - g) / 11)))

    day = int(i - ((year + int(year / 4) + i + 2 - c + int(c / 4)) % 7) + 28)
    month = 3

    if (day > 31):
        month = month + 1
        day = day - 31

    return date(year, month, day)


def getHolidays(year : int):
    holidays = []
    holidays.append(date(year, 1, 1))    # newyears
    holidays.append(date(year, 2, 24))   # independence day
    holidays.append(date(year, 5, 1))    # spring holiday
    holidays.append(date(year, 8, 30))   # second independence day
    holidays.append(date(year, 12, 24))  # christmas saturday
    holidays.append(date(year, 12, 25))  # first christmas holiday
    holidays.append(date(year, 12, 26))  # second christmas holiday
    
    easterSunday = calculateEasterSunday(year)
    goodFriday = easterSunday - timedelta(days=2)
    pentecost = easterSunday + timedelta(days=49)

    holidays.extend(list([goodFriday, easterSunday, pentecost]))

    return holidays