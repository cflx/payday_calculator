import sys
from argparse import ArgumentParser
from .calculator import Calculator
from datetime import date

def main():
    parser = ArgumentParser(prog='cli')
    parser.add_argument('--year', help="year to calculate", default=date.today().year)
    parser.add_argument('--filename', help="filename to output to")
    args = parser.parse_args()

    if (not str(args.year).isdigit()):
        print("Year must be a positive integer!")
        return

    if (not str(args.filename).isalnum()):
        print("Filename must be alphanumeric!")
        return

    if (args.filename is None):
        args.filename = args.year

    print("Calculaing dates for year %s, outputting to %s.csv!" % (args.year, args.filename))
    Calculator().calculate(int(args.year), str(args.filename))

if __name__ == '__main__':
    main()