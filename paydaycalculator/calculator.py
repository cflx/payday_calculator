from datetime import date, timedelta
from .holidays import getHolidays
import csv

class Calculator:

    def __init__(self):
        self.holidays = []
        return

    def __writeCsv(self, filename : str, reminders : list, paydays : list):
        with open(filename + '.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')

            writer.writerow(["Reminder date", "Payday date"])

            for i in range(0, reminders.__len__()):
                writer.writerow([reminders[i], paydays[i]])

        return

    def __isWorkDay(self, d):
        return d not in self.holidays and d.weekday() < 5

    def __calculateReminderDate(self, d):
        days = 0

        while (days != 3):
            d = d - timedelta(days=1)
            if (self.__isWorkDay(d)):
                days += 1

        return d


    def calculate(self, year : int, filename : str):
        self.holidays = getHolidays(year)
        
        reminderDates = []
        paydayDates = []

        for i in range(1,13):
            d = date(year, i, 10)
            
            while (not self.__isWorkDay(d)):
                d -= timedelta(days=1)

            reminderDates.append(self.__calculateReminderDate(d))
            paydayDates.append(d)

        self.__writeCsv(filename, reminderDates, paydayDates)
        return
