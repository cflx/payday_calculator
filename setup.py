from setuptools import setup, find_packages
import unittest

setup(
    name = 'payday_calculator',
    version = '0.1.0',
    packages = ['paydaycalculator'],
    test_suite='test',
    entry_points = {
        'console_scripts': [
            'paydaycalculator = paydaycalculator.__main__:main'
        ]
    })