# Payday Calculator #

Ranar Leissoni Spin TEK praktika testülesande lahendus.

## Kirjeldus
~~~sh 
Spin TEKis makstakse palka iga kuu kümnendal kuupäeval. Palka saab maksta ainult tööpäeval, seega kui 10. kuupäev langeb nädalavahetusele või riigipühale, siis makstakse palk välja sellele eelneval tööpäeval. Raamatupidaja soovib, et talle saadetaks meeldetuletus palga maksmise kohta kolm tööpäeva enne maksmise kuupäeva.

1.         Ülesandeks on kirjutada käsurearakendus (CLI rakendus), mille sisendiks on aastaarv ja väljundiks tabel, kus on valitud aasta iga kuu palgamaksmise kuupäev ja raamatupidajale meeldetuletuse saatmise kuupäev (kokku 12 rida ning tabeli päis).
2.         Rakendus peab väljundi kirjutama CSV-faili (näiteks "2022.csv").
~~~

## Kasutamine

1. Klooni
2. Ava süstemi CLI kaustas

Rakendust saab installida ja jooksutada PIP paketina:
~~~sh
> pip install -e .
> paydaycalculator --help
  usage: cli [-h] [--year YEAR] [--filename FILENAME]
  
  options:
  -h, --help           show this help message and exit
  --year YEAR          year to calculate
  --filename FILENAME  filename to output to
~~~

või jooksutada eraldiseisva moodulina
~~~sh
> py -m paydaycalculator --help
~~~

Testide jooksutamine: 
~~~sh
> py setup.py test
~~~
